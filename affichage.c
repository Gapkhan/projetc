/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  3                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   toutes les déclarations qui permet d'afficher                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Rochis kévin                                             *
*                                                                             *
*  Nom-prénom2 :     Fiche  Valentin                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  affichage.c                                              *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "affichage.h"

//permet de faire des saut de ligne (retour de ligne) autant de fois que le paramètre "Y" exige 
void line(int y){
    int i = 0;
    while ( i < y ) {
        printf("\n");
        i++;
    }
    
}

//Permet de faire l'affichage du menu et du choix des options
int choixduCesar() {
    int i = 0;
    
    while (i != 1 && i!=2 && i!=3) {
    menu();
    printf("1) chiffré un mot\n");
    printf("2) d'échiffré un mot\n");
    printf("3) sortir\n");
    line(22);
    printf("choisissez une option : ");
    scanf("%d",&i);
    videBuffer();
    }
    printf("Appuyer sur entrée...");
    
    return i;
    
}

//permet d'afficher l'entete du menu
void menu() {
    printf("************************************************************************************\n");
    printf("*                                      Menu                                        *\n");
    printf("************************************************************************************\n");
}

//permet d'afficher l'entete de Cryptage
void menuCrypt(){
    printf("************************************************************************************\n");
    printf("*                                    Cryptage                                      *\n");
    printf("************************************************************************************\n");
    
}

//permet d'afficher l'entete de Décryptage
void menudecrypt(){
    printf("************************************************************************************\n");
    printf("*                                   Décryptage                                     *\n");
    printf("************************************************************************************\n");
}

//permet d'afficher l'entete de Fin de programme
void finProg() {
    printf("************************************************************************************\n");
    printf("*                                 Fin de programme                                 *\n");
    printf("************************************************************************************\n");
}

//permet de faire une pause dans le programme et d'afficher ce qui permet de faire en fonction du paramètre i
void pause(int i){
    if (i == 1) {
        printf("Appuyer sur entrée pour continuer");
            getchar();
    } else if (i == 2) {
        line(25);
        printf("Appuyer sur entrée pour sortir du programme...");
    }
    
}


//permet de vider le buffer pour utiliser scanf a nouveau
void videBuffer() {
    int c ;
    while ( ((c = getchar() ) != '\n') && c != EOF){
            }
}
