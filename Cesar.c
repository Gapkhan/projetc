/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  3                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   permet d'utiliser le cryptage cesar                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Rochis kévin                                             *
*                                                                             *
*  Nom-prénom2 :     Fiche  Valentin                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  Cesar.c                                                  *
*                                                                             *
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Cesar.h"

//permet de décaler le message mis en paramètre de "cle" rang est d'écrire le résultat dans une variable messageCrypt mit en paramètre
void cesar (char * message , int cle , char * messageCrypt){
 
    int i = 0 ;
    int rang;
    
    while (message[i] != '\0') {
        if ( (message[i] >= 'a') && (message[i] <= 'z') ) {
            rang = (message[i] - 'a' + cle) %26;
            if (rang < 0 ) rang += 26;
            messageCrypt[i] = 'a' + rang;
    } else if (( message[i] >= 'A' ) && (message[i] <= 'Z')) {
            rang = (message[i] - 'A' + cle ) %26;
            if(rang<0) rang += 26;
            messageCrypt[i] = 'A' + rang;
        } else {
            messageCrypt[i] = message[i];
        }
        i++;
        
    }
    
    messageCrypt[i] = '\0';
    
}

//permet de décaler le message mis en paramètre de "-cle" rang est d'écrire le résultat dans une variable messageCrypt mit en paramètre
void cesarDechiffrement( char * messageCrypt , int cle , char * messageDeb) {
    cesar(messageCrypt, -cle , messageDeb);
}
