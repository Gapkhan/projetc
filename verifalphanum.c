/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  3                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   permet de vérifier les phrases saisies par l'utilisateur      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Rochis kévin                                             *
*                                                                             *
*  Nom-prénom2 :     Fiche  Valentin                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  verifalphanum.c                                          *
*                                                                             *
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "verifalphanum.h"
	 


// void convertirAccent(char *texte) {
// 	wchar_t TcaraSpecE[]=L"éèêë";
// 	wchar_t TcaraSpecA[]=L"àâä";
// 	wchar_t TcaraSpecI[]=L"îï";
// 	wchar_t TcaraSpecO[]=L"ôö";
// 	wchar_t TcaraSpecU[]=L"ùûü";
	
 
// 	for(int i=0;texte[i] !='\0';i++) {
		
// 		for(int j=0;TcaraSpecE[j]!='\0';j++) {
// 			if(texte[i]==TcaraSpecE[j]) {
// 				texte[i] = 'e';
// 			}
// 		}
// 	}
// 	for(int i=0;texte[i] !='\0';i++) {
// 		for(int j=0;TcaraSpecA[j]!='\0';j++) {
// 			if(texte[i]==TcaraSpecA[j]) {
				
// 				texte[i] = 'a';
// 			}
			
// 			}
// 		}
	
// 	for(int i=0;texte[i] !='\0';i++) {
// 		for(int j=0;TcaraSpecI[j]!='\0';j++) {
// 			if(texte[i]==TcaraSpecI[j]) {
// 				texte[i] = 'i';
// 			}
// 		}
// 	}
// 	for(int i=0;texte[i] !='\0';i++) {
// 		for(int j=0;TcaraSpecO[j]!='\0';j++) {
// 			if(texte[i]==TcaraSpecO[j]) {
// 				texte[i] = 'o';
// 			}
// 		}
// 	}
// 	for(int i=0;texte[i] !='\0';i++) {
// 		for(int j=0;TcaraSpecU[j]!='\0';j++) {
// 			if(texte[i]==TcaraSpecU[j]) {
// 				texte[i] = 'j';
// 			}
// 		}
// 	}

// }

//permet de vérifier si le texte mis en paramètre contient des caractères spéciaux
int verifCaraSpec(char *texte) {
	wchar_t TcaraSpec[]=L"é&(-_=çà~#èêëàâäîïôöùûü{[|'`^@]}¨$¤£ù%*µ!:/§.;,?+²<>)";
	int correct=1;  
	for(int i=0;texte[i] !='\0';i++) {
		for(int j=0;TcaraSpec[j]!='\0';j++) {
			if(texte[i]==TcaraSpec[j]) {
				
				correct = 0;
			}
		}
	}
	return correct;
}


