/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  3                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   permet de lancer le programme                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Rochis kévin                                             *
*                                                                             *
*  Nom-prénom2 :     Fiche  Valentin                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                   *
*                                                                             *
******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Cesar.c"
#include "verifalphanum.c"
#include "affichage.c"



void main()
{   
    int alpha = 0;
    int choix;
    int cle = 0;
    char message[100];
    char messageCrypt[100] ;
    char messageDeb[100] ;
    

    
        
 while (choix != 3 ) {
     choix = choixduCesar();
     videBuffer();
     if(choix ==1){
         line(25);
         menuCrypt();
         printf("Entrez la clé : ");
         scanf("%d" , &cle);
         videBuffer();
         printf("message a chiffré est : ");
         scanf("%[^\n]",&message);
         alpha = verifCaraSpec(message);
             if (alpha ==1) {
                 cesar(message,cle,messageCrypt);
                 printf("voici le message crypté : %s\n",messageCrypt);
             }else {
                 printf("Votre message comporte de(s) caractere(s) spéciaux !\n");
             }
             line(22);
             pause(1);
             
         }else if (choix == 2){
             line(25);
             menudecrypt();
             printf("Entrez la clé : ");
             scanf("%d" , &cle);
             videBuffer();
             printf("message a chiffré est : ");
             scanf("%[^\n]",&message);
             alpha = verifCaraSpec(message);
                 if (alpha ==1) {
                     cesarDechiffrement(message,cle,messageDeb);
                     printf("voici le message décripté : %s\n",messageDeb);
                 }else{
                     printf("Votre message comporte de(s) caractere(s) spéciaux !\n");
                 }
             line(22);
             pause(1);
         }else {
             finProg();
             pause(2);
         }
         videBuffer();
     }
    


}


